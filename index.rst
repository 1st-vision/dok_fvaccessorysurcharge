.. |label| replace:: 1st Vision Zubehör Zuschlag
.. |snippet| replace:: FvAccessorySurcharge
.. |Author| replace:: 1st Vision GmbH
.. |minVersion| replace:: 5.2.0
.. |maxVersion| replace:: 5.3.4
.. |version| replace:: 1.0.1
.. |php| replace:: 7.0


|label|
============

.. sectnum::

.. contents:: Inhaltsverzeichnis



Überblick
---------
:Author: |Author|
:PHP: |php|
:Kürzel: |snippet|
:getestet für Shopware-Version: |minVersion| bis |maxVersion|
:Version: |version|

Beschreibung
------------
Dieses Plugin fügt automatisch Zubehör Artikel dem Warenkorb hinzu wenn bestimmte Artikel in den Warenkorb gelegt werden. Diese Zubehör Artikel können vom Kunde weder gelöscht noch geändert werden. Der Preis und die Menge des Zubehör Artikels wird dabei über vordefinierte Berechnungsregeln ermittelt.

Frontend
--------
Wird ein Artikel in den Warenkorb gelegt, dann wird der Zubehör Artikel ebenso in den Warenkorb hinzugefügt.
Die Menge und der Preis wird je nach Faktor berechnet.

Die Zubehör Artikel werden im Shop versteckt und können nicht aufgerufen werden.

Der Zubehör Artikel müssen im Shop als Artikel existieren, aber in einer versteckten Kategorie sein.

Im Warenkorb wird jeder Zuschlag für jede Artikelposition einzeln berechnet werden.
Unterschiedliche Artikel mit dem selben Zuschlag werden nicht aufaddiert.
Ein Kauf-Artikel kann mehrmals in den Warenkorb gelegt werden, dann wird der Zuschlag auch aufaddiert.
Wird der Artikel entfernt, wird auch der Zuschlag entfernt.
Der Zuschlag selbst kann nie entfernt werden können.
Der Zuschlag wird immer als Position direkt unter dem Kauf-Artikel im Warenkorb sein.
 

Backend
-------

Grundeinstellungen
__________________
.. image:: FvAccessorySurcharge1.png
:Pfad zur Import Datei: Hier hinterlegen Sie den kompletten Pfad zur der Importdatei
:Zubehör Artikelnummer: Hier wählen Sie das Freitextfeld für die Zubehör Artikelnummer die beim Artikel steht
:Basismengeneinheit: Hier wählen Sie das Freitextfeld für die Basismengeneinheit die beim Artikel steht
:Mengenfaktor: Hier wählen Sie das Freitextfeld für den Mengenfaktor der beim Artikel steht


Textbausteine
_____________

keine


technische Beschreibung
------------------------
Nach Installation des Plugins wird zuerst ein ExtJS Fehler ausgegeben. Da in der Plugin Konfiguration
die verwendeten Freitextfelder ausgewählt werden können, ist es leider nicht möglich diesen
Fehler zu verhindern. Das Plugin nach der Installation deshalb aktivieren und das Backend einmal neu laden.

Anschließend kann das Plugin konfiguriert werden. Erst mit abgeschlossener und vollständiger Konfiguration 
ist das Plugin aktiv.

Das Plugin stellt ein Import Command bereit, über das XML-Dateien für das Plugin importiert werden können.
Eine Beispiel-Datei befindet sich im Plugin Ordner (example.xml). Existiert eine solche Datei an der im Plugin
konfigurierten Stelle, kann diese neben dem Shopware-eigenen Cronjob auch über das folgende Command importiert werden:

./bin/console first_vision:accessory_surcharge:import


Modifizierte Template-Dateien
-----------------------------
:/account/order_item_details.tpl:
:/checkout/ajax_cart.tpl:
:/checkout/finish_item.tpl:
:/checkout/items/product.tpl:




